FROM python:3.7-alpine

RUN apk add --no-cache nginx
RUN apk add --no-cache bash
RUN apk add --no-cache uwsgi
RUN apk add python3-dev build-base linux-headers pcre-dev

ENV PYTHONUNBUFFERED 1

COPY ./etc/nginx.conf /etc/nginx/nginx.conf
RUN mkdir -p /usr/share/nginx/html
RUN mkdir -p /run/nginx/

WORKDIR /usr/src/app/frontend/
COPY ./frontend/dist/frontend /usr/share/nginx/html

RUN mkdir -p /usr/src/app/backend

# Update working directory
WORKDIR /usr/src/app/backend

COPY ./backend /usr/src/app/backend/

RUN pip install pipenv
RUN pip install uwsgi
RUN pipenv install --system --deploy

COPY ./etc/uwsgi.ini /etc/uwsgi.ini

RUN chmod a+x bootstrap.sh

# run server
#CMD ./bootstrap.sh&

WORKDIR /usr/src/app
COPY ./etc/docker_entry.sh ./
RUN chmod a+x docker_entry.sh

EXPOSE 80

CMD ./docker_entry.sh

