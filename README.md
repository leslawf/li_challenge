# Eurostat graph - simple webapp

This is a simple Angular-Flask web application 

## Requirements 
angular6, python3.7, sqlite, pipevn 

## Running in docker (nginx + uwsgi)

* prepare sqlite3 database - run `# ./backend/create_database.sh`
* build frontend - go to `frontend`, run `npm install` and `ng build --prod`
* build docker `docker build -t eurostat .`  (root directory)
* run docker app `docker run -p80:80 eurostat`

## Running backend in develpoment mode:
* prepare sqlite db - run `backend/create_database.sh`
* go to `backend` and run `pipenv install --dev`
* running flask as backend: go to `backend` and run `./bootstrap.sh`
* running ng serve as frontend:  go to`frontend`. Run `npm install` and `ng build` and then `ng serve` 



