import { TestBed, inject } from '@angular/core/testing';

import { EurostatService } from './eurostat.service';

describe('EurostatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EurostatService]
    });
  });

  it('should be created', inject([EurostatService], (service: EurostatService) => {
    expect(service).toBeTruthy();
  }));
});
