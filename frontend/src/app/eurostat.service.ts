import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../environments/environment';
import {catchError, retry} from 'rxjs/operators';



export interface DeclarantsList {
  iso_code: string[];
}

export interface DeclarantStat {
  period: string[];
  value: number[];
  sma: number[];
  mom: number[];
  yoy: number[];
}

@Injectable({
  providedIn: 'root'
})
export class EurostatService {
  constructor(private http: HttpClient) { }

  getDeclarants(): Observable<DeclarantsList>{
    let obj = this.http.get<DeclarantsList>(`${environment.API_URL}/get_declarants`)
      .pipe(
        retry(3),
        catchError(this.handleError));
    return obj;
  }

  getDeclarantStat(daclarant: string, tradeType: string): Observable<DeclarantStat>{
    let obj = this.http.get<DeclarantStat>(`${environment.API_URL}/get_declarant_stat?declarant=${daclarant}&tradeType=${tradeType}`)
      .pipe(
        retry(3),
        catchError(this.handleError));
    return obj;
  }

  private handleError(error) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Uexpected error occurred. Please try again later.');
  }
}
