import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EurostatgraphComponent } from './eurostatgraph.component';

describe('EurostatgraphComponent', () => {
  let component: EurostatgraphComponent;
  let fixture: ComponentFixture<EurostatgraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EurostatgraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EurostatgraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
