import { Component, OnInit } from '@angular/core';
import { EurostatService } from '../eurostat.service';
import { DeclarantsList, DeclarantStat} from '../eurostat.service';
import { EChartOption } from 'echarts';


@Component({
  selector: 'app-eurostatgraph',
  templateUrl: './eurostatgraph.component.html',
  styleUrls: ['./eurostatgraph.component.css']
})

export class EurostatgraphComponent implements OnInit {
  //default values for
  declarant = 'PL';
  tradeType = 'I';

  declarants: string[] = [this.declarant];
  chartOption: EChartOption = {};

  errorOccured = false;
  errorMsg = "";

  constructor(private ess: EurostatService) { }

  ngOnInit() {
    this.getDeclarantStat();
    this.ess.getDeclarants().subscribe((item: DeclarantsList)  =>  this.declarants = item['iso_code']);
  }

  declarntChangeHandler() {
    this.getDeclarantStat();
  }

  getDeclarantStat() {
    this.ess.getDeclarantStat(this.declarant, this.tradeType).subscribe(
      (item: DeclarantStat) => this.reloadChart(item),
      error => {this.errorMsg = error; this.errorOccured = true;});
  }

  reloadChart(val) {
    const options = {};
    options['xAxis'] = [
      {type: 'category', data: val['period']},
      {type: 'category', data: val['period'], gridIndex: 1}];

    options['yAxis'] = [
      {type: 'value'},
      {type: 'value', gridIndex: 1}];

    options['legend'] = {data: ['total value', 'SMA', 'MoM', 'YoY']};

    options['grid'] = [
      {left: 130, right: 50, height: '45%', top: '5%'},
      {left: 130, right: 50, height: '30%', top: '59%'}];

    options['series'] = [
      {name: 'total value', data: val['value'], type: 'line', symbolSize: 4},
      {name: 'SMA', data: val['sma'], type: 'line', symbolSize: 4},
      {name: 'MoM', data: val['mom'], type: 'line', symbolSize: 4, xAxisIndex: 1, yAxisIndex: 1},
      {name: 'YoY', data: val['yoy'], type: 'line', symbolSize: 4, xAxisIndex: 1, yAxisIndex: 1}];

    options['dataZoom'] = [{realtime: true, start: 5, end: 95, height: '4%', bottom: 0, xAxisIndex: [0, 1]}];

    this.chartOption = options;
  }
}
