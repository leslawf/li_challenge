import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { EurostatgraphComponent } from './eurostatgraph/eurostatgraph.component';
import { NgxEchartsModule } from 'ngx-echarts';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    EurostatgraphComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxEchartsModule,
    NgSelectModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
