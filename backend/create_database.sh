#!/bin/bash
script_dir=$(dirname "$0")
echo "database will be created in ${script_dir}/src/eurostat.db"

#validate csv file
tmpfile=$(mktemp /tmp/XXXXXX)
cat $script_dir/eurostat.csv  | grep -e "^[1,2]\{1\}[0-9]\{3\}[1,0]\{1\}[0-9]\{1\}.*,.*[a-zA-Z]\{2\}.*,.*[a-zA-Z]\{2\}.*,.*[IE].*,.*[0-9\.]\+$" > $tmpfile
sqlite3 $script_dir/src/eurostat.db << EOF
DROP TABLE IF EXISTS eurostat;

CREATE TABLE eurostat(
  "PERIOD" TEXT,
  "DECLARANT_ISO" TEXT,
  "PARTNER_ISO" TEXT,
  "TRADE_TYPE" TEXT,
  "VALUE_EUR" REAL
);

.mode csv
.import $tmpfile eurostat

EOF

rm $tmpfile

echo done
