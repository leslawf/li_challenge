from flask import Flask, jsonify, request
from flask_cors import CORS
from .eurostat import Eurostat
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# sql database could be moved out to configuration file
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///eurostat.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
CORS(app)

eurostat = Eurostat(db)


@app.route('/')
def blank():
    return ""


@app.route('/get_declarants')
def get_declarants_list():
    return jsonify(eurostat.get_declarants_list())


@app.route('/get_declarant_stat')
def get_declarant_stat():
    return jsonify(eurostat.get_declarant_stat(declarant=request.args.get('declarant', 'PL'),
                                               trade_type=request.args.get('tradeType', 'I')))


if __name__ == "__main__":
    app.run()
