from .helpers import sma, popcomp, convert_date, param_validator
import sqlalchemy

from sqlalchemy import Column, String, and_, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.expression import label


Base = declarative_base()


class EurostatTable(Base):
    __tablename__ = "eurostat"

    period = Column(String(30), name='PERIOD', primary_key=True)
    declarant = Column(String(30), name='DECLARANT_ISO')
    partner = Column(String(30), name='PARTNER_ISO')
    trade_type = Column(String(30), name='TRADE_TYPE')
    value = Column(sqlalchemy.types.REAL, name='VALUE_EUR')


class Eurostat:
    def __init__(self, db):
        self.__db = db

    def get_declarants_list(self):
        c = self.__db.session.query(EurostatTable.declarant).distinct().order_by(EurostatTable.declarant.asc()).all()
        return {'iso_code': [it.declarant for it in c]}

    @sma('value')
    @popcomp('value', 'period', '%m/%Y')
    @convert_date('period', '%Y%m', '%m/%Y')
    def __get_declarant_stat_query(self, declarant='PL', trade_type='I'):
        q = self.__db.session.query(
            EurostatTable.period,
            label('value', func.sum(EurostatTable.value))
        ).filter(
            and_(
                EurostatTable.declarant == declarant,
                EurostatTable.trade_type == trade_type)
        ).group_by(
            EurostatTable.period
        ).order_by(EurostatTable.period.asc())

        for it in q.all():
            yield it._asdict()

    @param_validator(declarant='^[A-Z]{2}$', trade_type='^[A-Z]{1}$')
    def get_declarant_stat(self, declarant='PL', trade_type='I'):
        r = {'period': [], 'value': [], 'sma': [], 'mom': [], 'yoy': []}
        for x in self.__get_declarant_stat_query(declarant, trade_type):
            for k, v in r.items():
                if k in x:
                    v.append(x[k])
        return r








