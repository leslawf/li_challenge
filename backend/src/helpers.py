from functools import wraps
from datetime import datetime
import re
import logging


def convert_date(key, in_format, out_format):
    def decorator(f):
        @wraps(f)
        def convert_date_wrapper(*args, **kwargs):
            for x in f(*args, **kwargs):
                try:
                    x[key] = datetime.strptime(x[key], in_format).strftime(out_format)
                    yield x
                except:
                    logging.warning(f"Parsing date {in_format} from string: {x[key]} failed. Row {x} skipped.")
                    pass
        return convert_date_wrapper
    return decorator


def sma(key, outkey='sma', period=12):
    def decorator(f):
        @wraps(f)
        def sma_wrapper(*args, **kwargs):
            values = []

            gen = f(*args, **kwargs)
            for x in gen:
                values.append(x[key])
                if len(values) > period:
                    values.pop(0)

                if len(values) < period:
                    x[outkey] = None  # None is represented as null in Json (that's why not NaN is used here)
                else:
                    x[outkey] = sum(values) / period
                yield x
        return sma_wrapper
    return decorator


class PoPComp:
    def __init__(self):
        self.__data = {}
        self.__current = None
        pass

    # To avoid huge memory foot print some optimization could to be implemented
    # For test data there is no need to providing any optimization as data set is huge.
    def __get_value(self, target):
        if target not in self.__data:
            return None  # float('nan')
        c = self.__data[self.__current]
        p = self.__data[target]
        return (c/p-1) * 100

    def add(self, month, year, value):
        self.__current = (month, year)
        self.__data[self.__current] = value

    def yoy(self):
        target = (self.__current[0], self.__current[1]-1)
        return self.__get_value(target)

    def mom(self):
        tm = (self.__current[0]+11) % 12
        if tm == 0:
            target = (11, self.__current[1]-1)
        else:
            target = (tm, self.__current[1])

        return self.__get_value(target)


# period over period comparison
def popcomp(key, period_key, period_format, yoy_key='yoy', mom_key='mom'):
    def decorator(f):
        @wraps(f)
        def pop_wrapper(*args, **kwargs):

            e = PoPComp()
            for x in f(*args, **kwargs):
                dt = datetime.strptime(x[period_key], period_format)

                e.add(dt.month, dt.year, x[key])
                x[yoy_key] = e.yoy()
                x[mom_key] = e.mom()
                yield x

        return pop_wrapper
    return decorator


def param_validator(*args, **kwargs):
    def decorator(f):
        @wraps(f)
        def validator(*f_args, **f_kwargs):
            if len(f_args)-1 < len(args):
                raise Exception('wrapper args do not match function args')

            for k, v in enumerate(args):
                m = re.compile(v)
                if not m.match(f_args[k]):
                    raise Exception(f"args[{k}] doesn\'t match {v} rule")

            for k, v in kwargs.items():
                if k not in f_kwargs:
                    raise Exception('wrapper kwargs do not match function kwargs')

                m = re.compile(v)
                if not m.match(f_kwargs[k]):
                    raise Exception(f"kwargs[{k}] doesn\'t match {v} rule")

            return f(*f_args, **f_kwargs)
        return validator
    return decorator


