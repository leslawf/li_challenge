import unittest
from src.helpers import PoPComp, sma


class TestSMADecorator(unittest.TestCase):

    def testSMA(self):
        values = [{'v': x} for x in[1, 1, 1, 1, 5, 5, 5, 5, 1.5]]
        expected = [None, None, None, 1, 2, 3, 4, 5, 4.125]

        @sma(key='v', period=4)
        def get_values():
            for v in values:
                yield v

        for i, v in enumerate(get_values()):
            #print(f"{i}: value: {v}, expected: {expected[i]}")
            if expected[i] is None:
                self.assertTrue(v['sma'] is None)
            else:
                self.assertAlmostEqual(v['sma'], expected[i])


class TestOverComp(unittest.TestCase):

    def test_yoymom(self):
        oc = PoPComp()
        oc.add(1, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertTrue(oc.mom() is None)

        oc.add(2, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(3, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(4, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(5, 2000, 10)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 900)

        oc.add(6, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), -90)

        oc.add(7, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(8, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(9, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(10, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(11, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(12, 2000, 1)
        self.assertTrue(oc.yoy() is None)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(1, 2001, 1)
        self.assertAlmostEqual(oc.yoy(), 0)
        self.assertAlmostEqual(oc.mom(), 0)

        oc.add(2, 2001, 2)
        self.assertAlmostEqual(oc.yoy(), 100)
        self.assertAlmostEqual(oc.mom(), 100)

        oc.add(3, 2001, 3)
        self.assertAlmostEqual(oc.yoy(), 200)
        self.assertAlmostEqual(oc.mom(), 50)

        oc.add(4, 2001, 0.5)
        self.assertAlmostEqual(oc.yoy(), -50)
        self.assertAlmostEqual(oc.mom(), -83.3333333)

        oc.add(5, 2001, 11)
        self.assertAlmostEqual(oc.mom(), 2100)
        self.assertAlmostEqual(oc.yoy(), 10)

