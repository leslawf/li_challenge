#!/bin/bash

cd backend
uwsgi -i /etc/uwsgi.ini &
nginx -g 'daemon off;'
